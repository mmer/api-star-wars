const categories= [
    {text: "Personajes", url: "https://swapi.dev/api/people/"},
    {text: "Planetas", url: "https://swapi.dev/api/planets/"},
    {text: "Especies", url: "https://swapi.dev/api/species/"}
]

const main$$ = document.querySelector('main'); //Seleccionamos main, que ya está creado en el html

//Esta función es para crear la cabecera
function header() {
    const header$$ = document.createElement('header'); //Creamos un elemento header

    const imgHeader$$ = document.createElement('img'); //Creamos una imagen
    imgHeader$$.src='./assets/img/logo.jpg'; //Añadimos la src a nuestra imagen
    //imgHeader$$.setAttribute('src', './assets/img/logo.jpg'); es otra forma de añadir la src

    header$$.appendChild(imgHeader$$);  //Metemos la imagen dentro del header
    document.body.insertBefore(header$$,main$$);  //Metemos el header delante del elemento main
}

//Creamos un elemento a para enlazar links
function createAnchor(textLink, functionA) {  //Esta función la vamos a llamar más tarde
    const a$$ = document.createElement('a'); //Creamos un elemento a
    a$$.innerHTML = textLink; //Añadimos el texto que queremos dentro de <a></a>
    a$$.href='#'; //Al elemento a le añadimos href
    //a$$.setAttribute('href', '#');

    a$$.addEventListener('click', function(e){  //Cuando hagamos click en a, no queremos que haga nada, sino que llame a la función callback
        e.preventDefault();
        functionA();
    })
    return a$$;  //La función devuelve el a que hemos creado
}

//Esta función es para crear un main con un menu navegador

function main() {

    const nav$$ = document.createElement('div'); //Creamos un elemento main
    nav$$.classList.add('nav-menu'); //A este div, le añadimos la clase nav-menu

    //Recorremos el objeto data y vamos creando un a con el nombre que queramos
    for (i=0; i<categories.length; i++) {
        const text = categories[i].text; //Creamos una constante con el texto correspondiente para ir dentro del <a></a>
        const url = categories[i].url; //Creamos una constante con la url
        const a$$ = createAnchor(text, function() {  //Usamos la función createAnchor que habíamos creado antes
            
            //Esta función borra el contenido si hubiera y crea un nuevo contenido (div + articulo) y una tabla
            clearContainer('article');
            createContent(text, 'h1');
            createList(url);
        });

        nav$$.appendChild(a$$);
    }
    main$$.appendChild(nav$$);

    //Ahora creamos otro div dentro del main que contiene un articulo
    const div$$ = document.createElement('div');
    div$$.classList.add('content');
    const article$$ = document.createElement('article');

    div$$.appendChild(article$$);
    main$$.appendChild(div$$);
}

function clearContainer(element) {  //Recibe como parámetro lo que queremos eliminar
    document.querySelector(element).innerHTML = "";
}

//Esta función creará un elemento (le mandamos como parámetro cúal) dentro de un artículo con su texto correspondiente (se lo mandamos también como parámetro)

function createContent(h1Text, h$$) {
    const article$$ = document.querySelector('article');
    const h1$$ = document.createElement(h$$);
    h1$$.innerHTML = h1Text;

    article$$.appendChild(h1$$);
}

//Esta función creará una lista desordenada dentro del article con tantos elementos li como haya en el array al que llamamos con la API

function createList (url) {

    const article$$ = document.querySelector('article'); //Seleccionamos el articulo y creamos un elemento ul
    ul$$ = document.createElement('ul');

    fetch(url).then(res=>res.json())      
    .then(function(res){ //llamamos a la url y creamos un array con los objetos resultantes

        const arrayAPI = res.results; 

        for (i=0; i<arrayAPI.length; i++) {  //recorremos el array y añadimos un li con cada nombre

            const object = arrayAPI[i]; //creamos un objeto con cada elemento del array
            const li$$ = document.createElement('li');
            let name$$ = object.name;
            a$$ = createAnchor(name$$, function(){  //creamos un a con el nombre de cada objeto del array que hemos descargado de la url 
                ul$$.remove();   //eliminamos la lista que ya estaba para hacer una nueva
                createContent(name$$, 'h2');
                createSecondList(object);
            });
            li$$.appendChild(a$$);  //añadimos el <a></a> dentro de cada elemento de la lista 
            ul$$.appendChild(li$$);   // y cada li dentro de la <ul></ul>            
        }        
    })

    article$$.appendChild(ul$$);  //añadimos la lista al articulo para que se muestre en el documento
}

function createSecondList(object) { //aquí quiero recorrer el objeto para crear la sublista

    ul2$$ = document.createElement('ul'); 

    const keys = Object.keys(object);  //creo estos dos array para poder recorrerlos y coger solo los 6 primeros elementos. 
    const values = Object.values(object);

    for (j=1; j<7; j++) {
        li2$$ = document.createElement('li');
        li2$$.innerHTML = keys[j] + ': ' + values[j];
        ul2$$.appendChild(li2$$);

        const article$$ = document.querySelector('article');
        article$$.appendChild(ul2$$);
    }
}


// Cuando la página se cargue, llamará a estas dos funciones:

window.onload = function() {
    header();
    main();
}
